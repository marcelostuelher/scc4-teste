package br.com.scc4.teste.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class ServicesController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/listaReversa")
    public List listaReversa(@RequestParam List lista) {
        Collections.reverse(lista);
        return lista;
    }
    @RequestMapping("/imprimirImpares")
    public List<Integer> imprimirImpares(@RequestParam ArrayList<Integer> lista) {
        List<Integer> impares = new ArrayList<>();
        for(int i = 0; i < lista.size(); i++){
            if( lista.get(i) % 2 != 0   ) {
                impares.add(lista.get(i));
            }
        }
        return impares;
    }
    @RequestMapping("/imprimirPares")
    public List<Integer> imprimirPares(@RequestParam ArrayList<Integer> lista) throws Exception {
        List<Integer> impares = new ArrayList<>();
        for(int i = 0; i < lista.size(); i++){
            if( lista.get(i) % 2 == 0   ) {
                impares.add(lista.get(i));
            }
        }
        return impares;
    }
    @RequestMapping("/tamanho")
    public String tamanho(@RequestParam String palavra) throws Exception {
        String result = "tamanho = " + palavra.length();
        return result;
    }
    @RequestMapping("/maiusculas")
    public String maiusculas(@RequestParam String palavra) throws Exception {
        return palavra.toUpperCase();
    }
    @RequestMapping("/vogais")
    public String vogais(@RequestParam String palavra) throws Exception {
        char ch;
        String result = "";
        for(int i = 0; i < palavra.length(); i ++){
            ch = palavra.charAt(i);
            if(ch=='a'||ch=='e'||ch=='i'||ch=='o'||ch=='u'||ch=='A'||ch=='E'||ch=='I'||ch=='O'||ch=='U'){
               result += ch;
            }
        }
        return result;
    }
    @RequestMapping("/consoantes")
    public String consoantes(@RequestParam String palavra) throws Exception {
        char ch;
        String vogais = "";
        String consoantes ="";
        for(int i = 0; i < palavra.length(); i ++){
            ch = palavra.charAt(i);
            if(ch=='a'||ch=='e'||ch=='i'||ch=='o'||ch=='u'||ch=='A'||ch=='E'||ch=='I'||ch=='O'||ch=='U'){
                vogais += ch;
            } else {
                consoantes += ch;
            }
        }
        return consoantes;
    }
    @RequestMapping("/nomeBibliografico")
    public String nomeBibliografico(@RequestParam String nome) throws Exception {
        int lastNameIndex = nome.lastIndexOf("%");
        String lastName = nome.substring(lastNameIndex).toUpperCase().replace("%", "");
        String FirstName =  nome.substring(0, lastNameIndex).replace("%", " ");
        String result = lastName + ", " + FirstName;
        return result;
    }
    @RequestMapping("/saque")
        public String saque(@RequestParam int totalsaque) throws Exception {
            int LIMITE_MINIMO_SAQUE = 8;
            if (totalsaque < LIMITE_MINIMO_SAQUE) {
                throw new Exception("Saque inferior a "+LIMITE_MINIMO_SAQUE);
            }

            int[] result = { 0, 0 };
            int totsaque = totalsaque;
            int resto5 = totalsaque % 5;
            int tot3 = 0;
            int tot5 = totalsaque / 5;
            if (resto5 == 0) {
                result[0] = tot5;
            } else {
                for (int i = totsaque; i >= (LIMITE_MINIMO_SAQUE-1); --i) {
                    if (resto5 == 0) {
                        break;
                    } else {
                        totalsaque = totalsaque - 3;
                        tot3++;
                        tot5 = totalsaque / 5;
                        resto5 = totalsaque % 5;
                    }
                }
                result[0] = tot5;
                result[1] = tot3;
            }

            return "SAQUE: "+ result[0]+ " de notas de 5 e " + result[1]+ " notas de 3";

        }

    @RequestMapping("/criarRemetentes")
    public List<Remetente> criarRemetentes() throws Exception {
        List<Remetente> lista = new ArrayList<>();
        int i = 0;
        while(i < 10){
                    Remetente rm = new Remetente();
            rm.setCpf("0923952896"+i);
            rm.setEmail("marcelos@"+i+"teste.com");
            rm.setEndereco("Endereço remetente" + i);
            rm.setNome("Remetente" + i);
            rm.setTelefone("489880295"+i);
            lista.add(rm);
            i++;
        }
        return lista;
    }


    }