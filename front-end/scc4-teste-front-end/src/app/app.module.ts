import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'; 
//primeng
import {AccordionModule} from 'primeng/accordion';
import {TableModule} from 'primeng/table';
import { RemetentesComponent } from './components/remetentes/remetentes.component';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import { Remetente } from './models/remetente';
import {ButtonModule} from 'primeng/button';
import {InputMaskModule} from 'primeng/inputmask';
import {FileUploadModule} from 'primeng/fileupload';


@NgModule({
  declarations: [
    AppComponent,
    RemetentesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FileUploadModule,
    ButtonModule,
    HttpClientModule,
    TableModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    InputMaskModule,
    DropdownModule,
    DialogModule,
    InputTextModule,
    AccordionModule
  ],
  providers: [Remetente],
  bootstrap: [AppComponent]
})
export class AppModule { }
