import { Component, OnInit, ViewChild } from '@angular/core';
import { Remetente } from 'src/app/models/remetente';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-remetentes',
  templateUrl: './remetentes.component.html',
  styleUrls: ['./remetentes.component.scss']
})
export class RemetentesComponent implements OnInit {
  public cols;
  public remetentes = [];
  public remetenteNovo;
  public remetente: any = {};
  public displayDialog: boolean;
  public remetenteSelecionado: Remetente;
  public records: any[] = [];  
  constructor(private httpRequest: HttpClient) { }
  @ViewChild('csvReader', {static: true}) csvReader: any;
  ngOnInit() {
    this.cols = [
      { field: 'nome', header: 'Nome' },
      { field: 'telefone', header: 'Telefone' },
      { field: 'cpf', header: 'Cpf' },
      { field: 'endereco', header: 'Endereço' },
      { field: 'email', header: 'E-mail' }
    ];
    this.createRemetentes()
  }

  createRemetentes() {
    this.httpRequest.get("http://localhost:8080/criarRemetentes").subscribe((res: any) => {
      this.remetentes = res;
    }, e => {
      alert("não foi possivel listar os remetentes")
    })
  }
  showDialogToAdd() {
    this.remetenteNovo = true;
    this.remetente = {};
    this.displayDialog = true;
  }
  save() {
    let rms = [...this.remetentes];
    if (this.remetenteNovo)
      rms.push(this.remetente);
    else
      rms[this.remetentes.indexOf(this.remetenteSelecionado)] = this.remetente;

    this.remetentes = rms;
    this.remetente = null;
    this.displayDialog = false;
  }
  onRowSelect(event) {
    this.remetenteNovo = false;
    this.remetente = this.cloneRemetente(event.data);
    this.displayDialog = true;
  }
  cloneRemetente(c: any): any {
    let car = {};
    for (let prop in c) {
      car[prop] = c[prop];
    }
    return car;
  }
  uploadListener($event: any): void {
    let text = [];
    let files = $event.srcElement.files;

    if (this.isValidCSVFile(files[0])) {

      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);

        let headersRow = this.getHeaderArray(csvRecordsArray);

        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);
      };

      reader.onerror = function () {
        console.log('error is occured while reading file!');
      };

    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }
  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {  
    let csvArr = [];  
  
    for (let i = 1; i < csvRecordsArray.length; i++) {  
      let curruntRecord = (<string>csvRecordsArray[i]).split(',');  
      if (curruntRecord.length == headerLength) {  
        let csvRecord: Remetente = new Remetente();  
        csvRecord.nome = curruntRecord[0].trim();  
        csvRecord.telefone = curruntRecord[1].trim();  
        csvRecord.cpf = curruntRecord[2].trim();  
        csvRecord.endereco = curruntRecord[3].trim();  
        csvRecord.email = curruntRecord[4].trim();  
        console.log(csvRecord)
        this.remetentes.push(csvRecord);  
      }  
    }  
    return csvArr;  
  }
  isValidCSVFile(file: any) {
    return file.name.endsWith(".csv");
  }
  fileReset() {
    this.csvReader.nativeElement.value = "";
    this.records = [];
  }
  getHeaderArray(csvRecordsArr: any) {  
    let headers = (<string>csvRecordsArr[0]).split(',');  
    let headerArray = [];  
    for (let j = 0; j < headers.length; j++) {  
      headerArray.push(headers[j]);  
    }  
    return headerArray;  
  }
}

