import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RemetentesComponent } from './components/remetentes/remetentes.component';


const routes: Routes = [
  {path: "", component: RemetentesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
